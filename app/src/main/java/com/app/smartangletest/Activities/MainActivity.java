package com.app.smartangletest.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.smartangletest.Bases.BaseActivity;
import com.app.smartangletest.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;

    @Override
    public void set_layout() {
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {
        binding.task1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TaskOneActivity.class));
            }
        });
        binding.task2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TaskTwoActivity.class));
            }
        });
    }
}
