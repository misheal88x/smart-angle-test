package com.app.smartangletest.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.app.smartangletest.Adapters.MessageOptionsAdapter;
import com.app.smartangletest.Bases.BaseActivity;
import com.app.smartangletest.Bases.BaseFunctions;
import com.app.smartangletest.Models.MessageOptionObject;
import com.app.smartangletest.R;
import com.app.smartangletest.databinding.ActivityTaskOneBinding;

import java.util.ArrayList;
import java.util.List;

public class TaskOneActivity extends BaseActivity {

    private ActivityTaskOneBinding binding;
    private List<MessageOptionObject> list;
    private MessageOptionsAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    public void set_layout() {
        binding = ActivityTaskOneBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        list = new ArrayList<>();
        adapter = new MessageOptionsAdapter(this,list);
        layoutManager = new LinearLayoutManager(this);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(adapter);

        getOptions();
    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.messageMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getSelectedIndex() != -1) {
                    BaseFunctions.showSuccessToast(TaskOneActivity.this, "You selected : " + adapter.getSelectedIndex() + " option");
                }else{
                    BaseFunctions.showWarningToast(TaskOneActivity.this,getResources().getString(R.string.no_option_selected));
                }
            }
        });
    }
    private void getOptions(){
        list.add(new MessageOptionObject(getResources().getString(R.string.m_w_r),"2$"));
        list.add(new MessageOptionObject(getResources().getString(R.string.m),"2$"));
        list.add(new MessageOptionObject(getResources().getString(R.string.m_r_o),"2$"));
        adapter.notifyDataSetChanged();
    }

}