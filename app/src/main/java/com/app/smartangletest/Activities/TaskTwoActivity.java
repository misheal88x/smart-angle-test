package com.app.smartangletest.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;

import com.app.smartangletest.Bases.BaseActivity;
import com.app.smartangletest.Bases.BaseFunctions;
import com.app.smartangletest.R;
import com.app.smartangletest.databinding.ActivityTaskTwoBinding;
import com.github.dhaval2404.imagepicker.ImagePicker;

public class TaskTwoActivity extends BaseActivity {

    ActivityTaskTwoBinding binding;

    private String imagePath = "";
    @Override
    public void set_layout() {
        binding = ActivityTaskTwoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {
        binding.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(TaskTwoActivity.this).cropSquare().start();
            }
        });
        binding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePath.equals("")){
                    BaseFunctions.showWarningToast(TaskTwoActivity.this,getResources().getString(R.string.no_image));
                    return;
                }
                if (binding.fullName.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(TaskTwoActivity.this,getResources().getString(R.string.no_full_name));
                    binding.fullName.requestFocus();
                    return;
                }
                if (binding.email.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(TaskTwoActivity.this,getResources().getString(R.string.no_email));
                    binding.email.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(binding.email.getText().toString()).matches()){
                    BaseFunctions.showWarningToast(TaskTwoActivity.this,getResources().getString(R.string.error_string));
                    binding.email.requestFocus();
                    return;
                }
                if (binding.password.getText().toString().length() < 8){
                    BaseFunctions.showWarningToast(TaskTwoActivity.this,getResources().getString(R.string.no_password));
                    binding.password.requestFocus();
                    return;
                }
                BaseFunctions.showSuccessToast(TaskTwoActivity.this,getResources().getString(R.string.register_success));
            }
        });
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            imagePath = ImagePicker.Companion.getFilePath(data);
            binding.image.setImageURI(Uri.parse(imagePath));
        }
    }
}