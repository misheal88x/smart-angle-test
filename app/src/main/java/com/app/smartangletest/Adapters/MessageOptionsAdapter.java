package com.app.smartangletest.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.smartangletest.Models.MessageOptionObject;
import com.app.smartangletest.R;
import com.app.smartangletest.databinding.ItemMessageOptionBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class MessageOptionsAdapter extends RecyclerView.Adapter<MessageOptionsAdapter.ViewHolder>{
    private Context context;
    private List<MessageOptionObject> list;
    private ItemMessageOptionBinding binding;
    private int selected_index = -1;

    public MessageOptionsAdapter(Context context,List<MessageOptionObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemMessageOptionBinding view;

        public ViewHolder(ItemMessageOptionBinding b) {
            super(b.getRoot());
            view = b;
        }
        void bindData(MessageOptionObject item, int position) {
            view.type.setText(item.getType());
            view.value.setText(item.getValue());
            view.radio.setChecked(selected_index == position);

            view.radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        selected_index = position;
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemMessageOptionBinding.inflate(LayoutInflater.from(parent.getContext()),parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MessageOptionObject o = list.get(position);
        holder.bindData(o,position);

    }

    public int getSelectedIndex(){
        return selected_index;
    }
}
