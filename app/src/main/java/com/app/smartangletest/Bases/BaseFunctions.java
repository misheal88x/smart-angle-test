package com.app.smartangletest.Bases;

import android.app.Activity;
import android.content.Context;

import com.app.smartangletest.R;
import com.valdesekamdem.library.mdtoast.MDToast;

public class BaseFunctions {
    public static void runBackSlideAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    public static void showSuccessToast(Context context, String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS);
        mdToast.show();
    }

    public static void showWarningToast(Context context,String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING);
        mdToast.show();
    }
}
